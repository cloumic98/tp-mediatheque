import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import {JwtAuthenticationStrategy} from "./security/jwt.guard";
import {ConfigModule} from "@nestjs/config";
import { UserModule } from './user/user.module';

@Module({
  imports: [ConfigModule.forRoot(), DatabaseModule, UserModule],
  controllers: [],
  providers: [JwtAuthenticationStrategy],
})
export class AppModule {}
