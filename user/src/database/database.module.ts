import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {User} from "./entities/user.entity";

@Module({
    imports: [TypeOrmModule.forRootAsync({
        imports: [ConfigModule.forRoot()],
        inject: [ConfigService],
        useFactory: (config: ConfigService)  => ({
            type: 'postgres',
            host: config.get('BDD_HOST'),
            port: config.get('BDD_PORT'),
            database: config.get('BDD_DB'),
            username: config.get('BDD_USER'),
            password: config.get('BDD_PASSWORD'),
            entities: [User],
            synchronize: true
        })
    })]
})
export class DatabaseModule {}
