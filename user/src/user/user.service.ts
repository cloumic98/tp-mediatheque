import {BadRequestException, Injectable, Logger, UnauthorizedException} from '@nestjs/common';
import {JwtService} from "@nestjs/jwt";
import {User, UserRole} from "../database/entities/user.entity";
import {compare, hash} from "bcrypt";
import {RedisService} from "@mistercoookie/redis-pub-sub";
import {UserRegisterDTO} from "./dto/userRegister.dto";
import {UserLoginDTO} from "./dto/userLogin.dto";

@Injectable()
export class UserService {
    private logger: Logger = new Logger("UsersService")
    constructor(
        private jwtService: JwtService,
    ) {}

    async verifyToken(token: string) {
        const payload = this.jwtService.verify(token)
        const user = await User.findOne({
            where: {
                id: payload.userId
            },
            select: [ 'email', 'firstname', 'lastname', 'id', 'role', 'subscription', 'late' ]
        })
        return user
    }

    async register(userRegisterDTO: UserRegisterDTO) {
        const user = new User()

        user.email = userRegisterDTO.email
        user.lastname = userRegisterDTO.lastname
        user.firstname = userRegisterDTO.firstname

        user.password = await hash(userRegisterDTO.password, 10)

        await user.save()

        RedisService.publish<{email: string, firstname: string, lastname: string}>(
            'tj-register',
            {
                email: user.email,
                firstname: user.firstname,
                lastname: user.lastname
            }
        )

        return user;
    }

    async login(userLoginDTO: UserLoginDTO) {
        const user = await User.findOne({where: {email: userLoginDTO.email}})

        if (user == null)
            throw new BadRequestException("L'email ou le mot de passe fournit n'est pas correct")

        const isPasswordMatching = await compare(userLoginDTO.password, user.password)

        if (isPasswordMatching == false)
            throw new BadRequestException("L'email ou le mot de passe fournit n'est pas correct")

        const token = this.jwtService.sign({
            userId: user.id
        })

        return { token: token }
    }

    getAllUsers(user: User): Promise<User[]> {
        if (user.role != UserRole.PERSONNEL)
            throw new UnauthorizedException();

        return User.find({
            where: {
                role: UserRole.ADHERENT,
            },
            select: ['id', 'email', 'firstname', 'lastname', "role"],
        })
    }
}
