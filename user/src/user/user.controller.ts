import {Body, Controller, Get, Logger, Post, Req, UseGuards} from '@nestjs/common';
import {UserService} from "./user.service";
import {RedisPublication, RedisService} from "@mistercoookie/redis-pub-sub";
import {JwtAuthenticationGuard, RequestWithUser} from "../security/jwt.guard";
import {UserLoginDTO} from "./dto/userLogin.dto";
import {UserRegisterDTO} from "./dto/userRegister.dto";
import {User} from "../database/entities/user.entity";

@Controller('user')
export class UserController {
    private logger: Logger = new Logger('UsersController')

    constructor(
        private userService: UserService,
    ) {
        RedisService.subscribeChannel('tj-validation', (redisPublication: RedisPublication<string>) => {
            this.onUserValidation(redisPublication)
        })
    }

    async onUserValidation(redisPublication: RedisPublication<string>) {
        const user = await this.userService.verifyToken(redisPublication.publishedData)
        RedisService.publish(redisPublication.answerChannel, user)
    }

    @Post('register')
    register(@Body() userRegisterDTO: UserRegisterDTO) {
        return this.userService.register(userRegisterDTO)
    }

    @Post('login')
    login(@Body() userLoginDTO: UserLoginDTO) {
        return this.userService.login(userLoginDTO)
    }

    @UseGuards(JwtAuthenticationGuard)
    @Get('me')
    me(@Req() request: RequestWithUser) {
        return request.user
    }

    @UseGuards(JwtAuthenticationGuard)
    @Get()
    getUsers(@Req() request: RequestWithUser): Promise<User[]> {
        return this.userService.getAllUsers(request.user);
    }
}
