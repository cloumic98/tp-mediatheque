import { UserRole } from "src/entities/user.entity"

export class UserDTO {
    id: string
    lastName: string
    firstName: string
    email: string
    role: UserRole
}

interface RequestWithUser extends Request {
    user: UserDTO
}
export default RequestWithUser