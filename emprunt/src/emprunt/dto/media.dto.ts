import { MediaEnum } from "src/entities/media.entity";

export class CreateMediaDTO {
    type: MediaEnum
    name: string
    author: string
}