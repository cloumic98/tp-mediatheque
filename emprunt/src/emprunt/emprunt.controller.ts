import { Body, Controller, Get, Param, Post, Req, UseGuards } from "@nestjs/common";
import { ServiceAuthGuard } from "src/guard/auth.guard";
import { StaffAuthGuard } from "src/guard/staff.guard";
import RequestWithUser from "./dto/user.dto";
import { EmpruntService } from "./emprunt.service";

@UseGuards(ServiceAuthGuard)
@Controller('emprunt')
export class EmpruntController {

    constructor(
        private empruntService: EmpruntService
    ) {}

    @Post('/media/:idMedia') 
    createOneEmprunt(
        @Param('idMedia') idMedia: string,
        @Req() request: RequestWithUser
    ) {
        return this.empruntService.createOneEmprunt(idMedia, request.user)
    }

    @UseGuards(StaffAuthGuard)
    @Get('/media/:idMedia/emprunt/:idEmprunt')
    checkUserEmpruntMedia(
        @Param('idMedia') idMedia: string,
        @Param('idEmprunt') idEmprunt: string,
        @Req() request: RequestWithUser
    ) {
        return this.empruntService.checkUserEmpruntMedia(idMedia, idEmprunt, request.user)
    }

    @UseGuards(StaffAuthGuard)
    @Get('/media/:idMedia')
    checkAllMediaEmprunt(
        @Param('idMedia') idMedia: string,
    ) {
        return this.empruntService.checkAllMediaEmprunt(idMedia)
    }

    @UseGuards(StaffAuthGuard)
    @Get('/media/:idMedia/emprunt/:idEmprunt/return-date')
    checkUserEmpruntMediaReturnDate(
        @Param('idMedia') idMedia: string,
        @Param('idEmprunt') idEmprunt: string,
        @Req() request: RequestWithUser
    ) {
        return this.empruntService.checkUserEmpruntMediaReturnDate(idMedia, idEmprunt, request.user)
    }

    @UseGuards(StaffAuthGuard)
    @Get('user/:idUser')
    checkAllEmpruntByUser(@Param('idUser') idUser: string) {
        return this.empruntService.checkAllEmpruntByUser(idUser)
    }

    @Post()
    bringBackEmprunt(@Req() request: RequestWithUser) {
        return this.empruntService.bringBackEmprunt(request.user)
    }

}