import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Emprunt } from "src/entities/emprunt.entity";
import { Media } from "src/entities/media.entity";
import { User } from "src/entities/user.entity";
import { ServiceAuthStrategy } from "src/guard/auth.guard";
import { StaffAuthStrategy } from "src/guard/staff.guard";
import { EmpruntController } from "./emprunt.controller";
import { EmpruntService } from "./emprunt.service";

@Module({
    imports: [TypeOrmModule.forFeature([Media, User, Emprunt])],
    exports: [EmpruntService],
    providers: [StaffAuthStrategy, ServiceAuthStrategy, EmpruntService],
    controllers: [EmpruntController]
})
export class EmpruntModule { }