import { BadRequestException, Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Emprunt } from "src/entities/emprunt.entity";
import { Media } from "src/entities/media.entity";
import { User } from "src/entities/user.entity";
import { Repository } from "typeorm";
import { UserDTO } from "./dto/user.dto";
import * as moment from 'moment';
import { RedisService } from '@mistercoookie/nestjs-redis-pub-sub';
import { Cron } from '@nestjs/schedule';


@Injectable()
export class EmpruntService {

    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Media)
        private mediaRepository: Repository<Media>,
        @InjectRepository(Emprunt)
        private empruntRepository: Repository<Emprunt>,
    ) { }

    async createOneEmprunt(idMedia: string, user: UserDTO) {
        const media = await this.mediaRepository.findOne({
            where: {id: idMedia}
        })
        if(!media) {
            throw new NotFoundException('Media not found')
        }

        let createdDate = moment().format()
        let returnDate = moment().add(14, 'day').format()

        const emprunt = await this.empruntRepository.create({
            created_at: createdDate,
            return_at: returnDate,
            user: user,
            media: media
        })

        if(media.emprunted == true) {
            throw new BadRequestException('this media is already emprunted')
        }

        RedisService.publish('tj-emprunt-media', {
            idMedia: idMedia,
            title: media.name,
            createdAt: createdDate,
            returnAt: returnDate,
            user: user
        })

        RedisService.publish('tj-emprunt-subscription', {
            emprunt: emprunt
        })

        await this.empruntRepository.save(emprunt)
        return emprunt
    }

    async checkUserEmpruntMedia(
        idMedia: string,
        idEmprunt: string,
        user: UserDTO
    ) {
        const emprunt = await this.empruntRepository.findOne({
            relations: ['media', 'user'],
            where: {
                media: {
                    id: idMedia
                },
                id: idEmprunt
            }
        })

        if(!emprunt) {
            throw new NotFoundException('Emrunt not found')
        }

        return emprunt.user
    }

    async checkAllMediaEmprunt(idMedia: string) {
        return this.empruntRepository.find({
            relations: ['media'],
            where: {
                media: {
                    id: idMedia
                }
            }
        })
    }

    async checkUserEmpruntMediaReturnDate(
        idMedia: string,
        idEmprunt: string,
        user: UserDTO
    ) {
        const emprunt = await this.empruntRepository.findOne({
            relations: ['media'],
            where: {
                media: {
                    id: idMedia
                },
                id: idEmprunt
            }
        })

        if(!emprunt) {
            throw new NotFoundException('Emrunt not found')
        }

        return {
            return_at: emprunt.return_at
        }
    }

    @Cron('59 15 * * *')
    async check() {
        const date = moment()
        const check = moment().add(7, 'days')
        const emprunts = await this.empruntRepository.find({
            relations: ['media', 'user']
        })
        emprunts.forEach((emprunt) => {
            if(check.date() == (moment(emprunt.return_at).date())) {
                RedisService.publish('tj-return-media', {
                    idMedia: emprunt.media.id,
                    title: emprunt.media.name,
                    createdAt: emprunt.created_at,
                    returnAt: emprunt.return_at,
                    user: emprunt.user
                })
            } if (
                date.isBefore(moment(emprunt.return_at)) &&
                check.isAfter(moment(emprunt.return_at)) &&
                emprunt.media.emprunted == true
              ) {
                RedisService.publish('tj-return-media-late', {
                    idMedia: emprunt.media.id,
                    title: emprunt.media.name,
                    createdAt: emprunt.created_at,
                    returnAt: emprunt.return_at,
                    user: emprunt.user
                })
            }
        }) 
    }

    @Cron('0 16 * * *')
    async checkPersonnel() {
        const date = moment()
        const emprunts = await this.empruntRepository.find({
            relations: ['media', 'user']
        })
        let list = []
        emprunts.forEach((emprunt) => {
            if(date.isAfter((moment(emprunt.return_at)))) {
                emprunt.user.late ++
                list.push(emprunt)
                RedisService.publish('tj-return-media-personnel', {
                    list: list
                })
            }
        }) 
    }

    checkAllEmpruntByUser(idUser: string) {
        return this.empruntRepository.find({
            relations: ['user', 'media'],
            where: {
                user: {
                    id: idUser
                }
            }
        })
    }

    async bringBackEmprunt(user: UserDTO) {
        const emprunt = await this.empruntRepository.findOne({
            relations: ['media', 'user'],
            where: {
                user: {
                    id: user.id
                }
            }
        })

        if(!emprunt) {
            throw new NotFoundException('emprunt not found')
        }

        const media = await this.mediaRepository.findOne({
            where: {id: emprunt.media.id}
        })

        media.emprunted = false

        await this.mediaRepository.save(media)
        return emprunt
    }
}