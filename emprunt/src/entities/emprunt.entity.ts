import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Media } from "./media.entity";
import { User } from "./user.entity";

@Entity()
export class Emprunt {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column({
        type: 'varchar'
    })
    created_at: string

    @Column({
        type: 'varchar'
    })
    return_at: string

    @ManyToOne(()=> User, (user)=> user.emprunts)
    user: User

    @ManyToOne(()=> Media, (media)=> media.emprunts)
    media: Media
}