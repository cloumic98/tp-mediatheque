import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Emprunt } from "./emprunt.entity";

export enum MediaEnum {
    BOOK = "BOOK",
    MUSIC = "MUSIC"
}

@Entity()
export class Media {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column({
        type: 'enum',
        enum: MediaEnum
    })
    type: MediaEnum

    @Column({
        type: 'varchar'
    })
    name: string

    @Column({
        type: 'varchar'
    })
    author: string

    @Column({
        type: 'boolean',
        default: false
    })
    emprunted: boolean

    @OneToMany(()=> Emprunt, (emprunt)=> emprunt.media)
    emprunts: Emprunt[]
}