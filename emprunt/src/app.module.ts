import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DataBaseModule } from './database/database.module';
import { EmpruntModule } from './emprunt/emprunt.module';
import { ScheduleModule } from '@nestjs/schedule';


@Module({
  imports: [
    DataBaseModule,
    EmpruntModule,
    ScheduleModule.forRoot()
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
