# TP Médiatheque Vendredi 03/02/2023

Sprint TP Médiatheque

Groupe: 
- Jordan PICANT
- Théo THIERRY

## Présentation du sujet

La médiathèque Saint-Bernard lance un appel d'offres pour réaliser son nouveau système de gestion. Actuellement les membres du personnel tiennes la liste de leurs média et des adhérents sur un registre en papier. L'équipe aimerait informatiser cette gestion. Cette informatisation aura pour objectif de faire gagner du temps à l'équipe et aux adhérents.

## Installation

Clone la repository.
```shell
git clone https://gitlab.com/cloumic98/tp-mediatheque.git
```

Run 'npm install' dans tous les projets nestjs.
```shell
npm install
```

Crée un fichier .env a la racine de chaque projet nestjs et suivre le exemple .env.example pour savoir quoi écrire dans chaque fichier.

Après ca vous pouvez démarrer chaque projet.
```shell
npm run start:dev
```

## BDD

Vous pouvez trouver le fichier mysql workbench a la racine de la repository sous le nom [mediatheque_workbench](./mediatheque_workbench.mwb).

![bdd](./mediatheque_bdd_image.png)

## Choix technique et GANTT

Ceux-ci ont été fait sur Figma avec un Figjam, cliquez sur le lien ci-dessous pour consulter le Figjam.

[Figma Link](https://www.figma.com/file/ZGyf6SRkKCYAnu1DYHY8Hc/Untitled?node-id=0%3A1&t=UsyeU3y9FmlHB7Pg-1) ⬅⬅⬅

### Choix techno

![bdd](./choix_techno.png)

### BDD

![bdd](./matrice_bdd.png)

### pub/sub

![bdd](./matrice_pub_sub.png)

### GANTT

![GANTT](./GANTT.png)