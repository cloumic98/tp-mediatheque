import { Injectable } from "@nestjs/common";
import { RedisService, RedisPublication } from '@mistercoookie/nestjs-redis-pub-sub';
import { InjectRepository } from "@nestjs/typeorm";
import { Emprunt } from "src/entities/emprunt.entity";
import { Media } from "src/entities/media.entity";
import { User, UserSubscription } from "src/entities/user.entity";
import { Repository } from "typeorm";
import * as moment from 'moment'

@Injectable()
export class SubscriptionService {

    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Media)
        private mediaRepository: Repository<Media>,
        @InjectRepository(Emprunt)
        private empruntRepository: Repository<Emprunt>,
    ) {
        RedisService.subscribeChannel('tj-emprunt-subscription', (redisPublication: RedisPublication<Emprunt>) => {
            this.changeSubscriptionUser(redisPublication.publishedData)
        })
    }

    async changeSubscriptionUser(data: Emprunt) {
        console.log(data) 
        const emprunts = await this.empruntRepository.find({
            relations: ['user', 'media'],
            where: {
                user: {
                    id: data.user.id
                }
            }
        })

        if(emprunts.length > 3) {
            let empruntDate = 0
            emprunts.forEach((emprunt) => {
                empruntDate += moment(emprunt.return_at).date()
            })
            if(empruntDate < 31) {
                data.user.subscription = UserSubscription.DECOUVERTE
            }
        }
        if(emprunts.length > 10) {
            let empruntDate = 0
            emprunts.forEach((emprunt) => {
                empruntDate += moment(emprunt.return_at).date()
            })
            if(empruntDate < 61) {
                data.user.subscription = UserSubscription.AGUERRI
            }
        }
        if(emprunts.length > 20) {
            let empruntDate = 0
            emprunts.forEach((emprunt) => {
                empruntDate += moment(emprunt.return_at).date()
            })
            if(empruntDate < 31) {
                data.user.subscription = UserSubscription.CHEVRONNE
            }
        }
        if(data.user.subscription == UserSubscription.AGUERRI) {
            if(data.user.late == 3) {
                data.user.late = 0
                data.user.subscription = UserSubscription.DECOUVERTE
            }
        }
        if(data.user.subscription == UserSubscription.CHEVRONNE) {
            if(data.user.late == 5) {
                data.user.late = 0
                data.user.subscription = UserSubscription.AGUERRI
            }
        }

        await this.empruntRepository.save(data)
    }
}