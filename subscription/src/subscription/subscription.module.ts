import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Emprunt } from "src/entities/emprunt.entity";
import { Media } from "src/entities/media.entity";
import { User } from "src/entities/user.entity";
import { SubscriptionService } from "./subscription.service";

@Module({
    imports: [TypeOrmModule.forFeature([Media, User, Emprunt])],
    exports: [SubscriptionService],
    providers: [SubscriptionService]
})
export class SubscriptionModule { }