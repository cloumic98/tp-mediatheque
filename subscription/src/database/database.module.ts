import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { Media } from 'src/entities/media.entity';
import { User } from 'src/entities/user.entity';
import { Emprunt } from 'src/entities/emprunt.entity';

@Module({
    imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: (configService: ConfigService) => ({
                type: 'postgres',
                host: configService.get('POSTGRESQL_ADDON_HOST'),
                port: configService.get('POSTGRESQL_ADDON_PORT'),
                username: configService.get('POSTGRESQL_ADDON_USER'),
                password: configService.get('POSTGRESQL_ADDON_PASSWORD'),
                database: configService.get('POSTGRESQL_ADDON_DB'),
                entities: [Media, User, Emprunt],
                synchronize: true,
                migrations: ['dist/src/db/migrations.ts'],
                cli: { migrationsDir: 'src/db/migrations' },
            }),
        }),
    ],
})
export class DataBaseModule {}
