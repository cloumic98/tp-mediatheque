import { Injectable } from '@nestjs/common';
import {emailType} from "./dto/email.type";

const nodemailer = require("nodemailer");
const transport = nodemailer.createTransport({
  host: "sandbox.smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "389a00165df080",
    pass: "54f6700b308ea2"
  }
});

@Injectable()
export class AppService {

  sendEmail(options: emailType) {
    return transport.sendMail({
          from: '"hotel register" <register@hotel.com>',
          to: options.target,
          subject: "Bienvenue parmis nous",
          html: options.htmlMessage
    });
  }
}
