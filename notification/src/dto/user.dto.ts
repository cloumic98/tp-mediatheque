export type UserDTO = {
    lastname: string
    firstname: string
    email: string
    role: string
}
