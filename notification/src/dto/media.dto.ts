import {UserDTO} from "./user.dto";

export type MediaDto = {
    idMedia: string,
    title: string,
    createdAt: string,
    returnAt: string,
    user: UserDTO,
}
