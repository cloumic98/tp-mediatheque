import {UserDTO} from "./user.dto";

export type EmpruntDto = {
    id: string,
    created_at: string,
    return_at: string,
    user: UserDTO,
    media: {
        id: string,
        type: 'BOOK' | 'MUSIC',
        name: string,
        author: string,
        emprunted: boolean
    }
}


export type EmpruntRetardDto = {
    list: EmpruntDto[]
}
