import {Controller} from '@nestjs/common';
import {AppService} from './app.service';
import {RedisPublication, RedisService} from "@mistercoookie/redis-pub-sub";
import {UserDTO} from "./dto/user.dto";
import {MediaDto} from "./dto/media.dto";
import * as moment from "moment";
import {EmpruntDto, EmpruntRetardDto} from "./dto/mediaPersonnel.dto";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {

    RedisService.subscribeChannel('tj-register', async (redisPublication: RedisPublication<UserDTO>) => {
      this.appService.sendEmail({
        target: redisPublication.publishedData.email,
        htmlMessage: `<p>Bonjour ${redisPublication.publishedData.firstname} </p><p>Bienvenue parmis nos client</p>`,
      });
    })

    RedisService.subscribeChannel('tj-emprunt-media', async (redisPublication: RedisPublication<MediaDto>) => {
      const data = redisPublication.publishedData
      const createdAt = moment(data.createdAt).locale('fr')
      const returnAt = moment(data.returnAt).locale('fr')

      this.appService.sendEmail({
        target: data.user.email,
        htmlMessage: `<p>Bonjour ${data.user.firstname} </p><p>Vous avez emprunter le média ${data.title}, ${createdAt.calendar()}, vous devrez le rendre le ${returnAt.calendar()}</p>`,
      });
    })

    RedisService.subscribeChannel('tj-return-media-late', async (redisPublication: RedisPublication<MediaDto>) => {
      const data = redisPublication.publishedData
      const createdAt = moment(data.createdAt).locale('fr')
      const returnAt = moment(data.returnAt).locale('fr')

      this.appService.sendEmail({
        target: data.user.email,
        htmlMessage: `<p>Bonjour ${data.user.firstname}</p><p>Pour rappel, vous avez emprunter le média ${data.title}, il n'as pas été rendu avant le ${returnAt.format('lll')}, merci de le rendre au plus vite.</p>`,
      });
    })

    RedisService.subscribeChannel('tj-return-media', async (redisPublication: RedisPublication<MediaDto>) => {
      const data = redisPublication.publishedData
      const returnAt = moment(data.returnAt).locale('fr')

      this.appService.sendEmail({
        target: data.user.email,
        htmlMessage: `<p>Bonjour ${data.user.firstname}</p><p>Vous avez emprunter le média ${data.title}, il nous doit être retourner avant le ${returnAt.format('lll')}, merci de votre compréhension</p>`,
      });
    })

    RedisService.subscribeChannel('tj-return-media-personnel', async (redisPublication: RedisPublication<EmpruntRetardDto>) => {
      const data = redisPublication.publishedData.list

      const formatedForMail = data.map((value: EmpruntDto) => {
        return { returnAt: value.return_at, title: value.media.name, user: value.user }
      })

      this.appService.sendEmail({
        target: 'personnel@mediatheque.com',
        htmlMessage: `<p>Liste des média non rendu</p><p><ul>${
          formatedForMail.map((value: { returnAt: string, title: string, user: UserDTO }) => {
            return `<li>${value.user.firstname} ${value.user.lastname} (${value.user.email}): ${value.title}, ${moment(value.returnAt).locale('fr').format('lll')}</li>`
          }).join('')
        }</ul></p>`,
      });
    })
  }
}
