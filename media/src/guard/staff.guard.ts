import { RedisService } from "@mistercoookie/nestjs-redis-pub-sub";
import { ForbiddenException, Injectable, Logger, Req } from "@nestjs/common";
import { AuthGuard, PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-custom";
import { User } from "src/entities/user.entity";
import { UserDTO } from "src/media/dto/user.dto";

@Injectable()
export class StaffAuthGuard extends AuthGuard('staff') {}

@Injectable()
export class StaffAuthStrategy extends PassportStrategy(Strategy, 'staff') {
    private logger: Logger = new Logger('ServiceAuthStrategy')
    constructor( ) {
        super()
    }

    async validate(@Req() request) {
        if (request.headers.hasOwnProperty('authorization') == false) {
            return false
        }

        // Authorization : Bearer header.payload.sign
        const tokenParts = request.headers['authorization'].split(' ')
        if(tokenParts.length != 2){
            return false
        }

        const user: any = await RedisService.publishWithAnswer<string, any>(
            'tj-validation',
            tokenParts[1]
        )

        if(user.role != 'personnel') {
            throw new ForbiddenException('ok mec')
        }

        return user
    }
}
