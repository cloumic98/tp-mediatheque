import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

export enum MediaEnum {
    BOOK = "BOOK",
    MUSIC = "MUSIC"
}

@Entity()
export class Media {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column({
        type: 'enum',
        enum: MediaEnum
    })
    type: MediaEnum

    @Column({
        type: 'varchar'
    })
    name: string

    @Column({
        type: 'varchar'
    })
    author: string

    @Column({
        type: 'boolean',
        default: false
    })
    emprunted: boolean
}