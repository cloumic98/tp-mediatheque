import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"
import { Exclude } from 'class-transformer'

export enum UserRole {
    PERSONNEL = "personnel",
    ADHERENT = "adherent"
}

export enum UserSubscription {
    DECOUVERTE = "DECOUVERTE",
    AGUERRI = "AGUERRI",
    CHEVRONNE = "CHEVRONNE"
}
@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column('varchar', { unique: true })
    email: string

    @Exclude()
    @Column('varchar')
    password: string

    @Column('varchar')
    firstname: string

    @Column('varchar')
    lastname: string

    @Column({
        type: 'enum',
        enum: UserRole,
        default: UserRole.ADHERENT,
    })
    role: UserRole;

    @Column({
        type: 'float'
    })
    late: number

    @Column({
        type: 'enum',
        enum: UserSubscription,
        default: null
    })
    subscription: UserSubscription
}