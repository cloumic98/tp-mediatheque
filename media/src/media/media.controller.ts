import { Body, Controller, Get, Param, Post, Req, UseGuards } from "@nestjs/common";
import { ServiceAuthGuard } from "src/guard/auth.guard";
import { StaffAuthGuard } from "src/guard/staff.guard";
import { CreateMediaDTO } from "./dto/media.dto";
import RequestWithUser from "./dto/user.dto";
import { MediaService } from "./media.service";
import { RedisService, RedisPublication } from '@mistercoookie/nestjs-redis-pub-sub';

@UseGuards(ServiceAuthGuard)
@Controller('media')
export class MediaController {

    constructor(
        private mediaService: MediaService
    ) { 
        RedisService.subscribeChannel('tj-emprunt-media', (redisPublication: RedisPublication<string>) => {
            this.changeEmpruntState(redisPublication.publishedData)
        })
    }

    @UseGuards(StaffAuthGuard)
    @Post()
    createMedia(@Req() request: RequestWithUser, @Body() data: CreateMediaDTO) {
        return this.mediaService.createOneMedia(request.user, data)
    }

    @UseGuards(StaffAuthGuard)
    @Get()
    getAllMedia() {
        return this.mediaService.getAllMedia()
    }

    @UseGuards(StaffAuthGuard)
    @Get('emprunted/:idMedia')
    checkIfMediaEmprunted(@Param('idMedia') idMedia: string) {
        return this.mediaService.checkIfMediaIsEmprunted(idMedia)
    }

    changeEmpruntState(data: any) {
        return this.mediaService.changeEmpruntState(data.idMedia)
    }
}