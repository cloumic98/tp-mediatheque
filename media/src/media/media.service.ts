import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Media } from "src/entities/media.entity";
import { Repository } from "typeorm";
import { CreateMediaDTO } from "./dto/media.dto";
import { UserDTO } from "./dto/user.dto";

@Injectable()
export class MediaService {

    constructor(
        @InjectRepository(Media)
        private mediaRepository: Repository<Media>
    ) { }

    async getAllMedia() {
        return this.mediaRepository.find()
    }


    async createOneMedia(user: UserDTO, data: CreateMediaDTO) {
        const media = await this.mediaRepository.create({
            ...data
        })

        await this.mediaRepository.save(media)
        return media
    }

    async checkIfMediaIsEmprunted(idMedia: string) {
        const media = await this.mediaRepository.findOne({
            where: { id: idMedia }
        })

        if(!media) {
            throw new NotFoundException('Media not found')
        }

        if(media.emprunted == true) {
            return true
        }else{
            return false
        }
    }

    async changeEmpruntState(idMedia: string) {
        const media = await this.mediaRepository.findOne({
            where: { id: idMedia }
        })

        if(!media) {
            throw new NotFoundException('Media not found')
        }

        if(media.emprunted == true) {
            media.emprunted = false
            await this.mediaRepository.save(media)
        }else {
            media.emprunted = true
            await this.mediaRepository.save(media)
        }
        return media
    }

}