import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Media } from "src/entities/media.entity";
import { User } from "src/entities/user.entity";
import { ServiceAuthStrategy } from "src/guard/auth.guard";
import { StaffAuthStrategy } from "src/guard/staff.guard";
import { MediaController } from "./media.controller";
import { MediaService } from "./media.service";

@Module({
    imports: [TypeOrmModule.forFeature([Media, User])],
    exports: [MediaService],
    providers: [ServiceAuthStrategy, MediaService, StaffAuthStrategy],
    controllers: [MediaController]
})
export class MediaModule { }